#include <gavl/gavl.h>
#include <stdio.h>

int main() {
	int i;
	for(i=0; i<gavl_num_sample_formats(); i++) {
		gavl_sample_format_t fmt=gavl_get_sample_format(i);
		printf("%d: %s[%d]\n", i, gavl_sample_format_to_string(fmt), fmt);
	}

	return 0;
}
